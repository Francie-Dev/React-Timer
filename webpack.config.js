var webpack = require('webpack');

module.exports = {
  entry: './app/index.jsx',
  output: {
    path: __dirname,
    filename: './public/bundle.js'
  },
  resolve: {
    root: __dirname,
    alias: {
      App: 'app/components/App.jsx',
      Nav: 'app/components/Nav.jsx',
      Coundown: 'app/components/Coundown.jsx',
      CoundownForm: 'app/components/CoundownForm.jsx',
      Timer: 'app/components/Timer.jsx',
      Clock: 'app/components/Clock.jsx',
      Controls: 'app/components/Controls.jsx'
    },
    extensions: ['', '.js','.jsx']
  },
  module: {
    loaders: [
      {
        loader: 'babel-loader',
        query: {
          presets:['react', 'es2015','stage-2']
        },
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/
      }
    ]
  }
  devtool: 'cheap-module-eval-source-map'
};
