import React from 'react';

class Controls extends React.Component {

  onStatusChange = (newStatus) => {
    return () => {
      this.props.onStatusChange(newStatus);
    }
  }
  componentWillReceiveProps = (nextProps) => {
    console.log('componentWillReceiveProps,' + nextProps.countdonwnStatus);
  }

   render(){

     var {countdonwnStatus} = this.props;

     var renderStartStopButton = () =>{
       if (countdonwnStatus === 'started') {
         return <button onClick={this.onStatusChange('paused')}>Pause</button>
       }else if (countdonwnStatus === 'paused'){
         return <button onClick={this.onStatusChange('started')}>Start</button>
       }
     };

     return(
       <div>
         {renderStartStopButton()}
         <button onClick={this.onStatusChange('stopped')}>Clear</button>
       </div>
   )
   }

};
Controls.propTypes = {
    countdonwnStatus: React.PropTypes.string.isRequired,
    onStatusChange: React.PropTypes.func.isRequired
}

export default Controls;
