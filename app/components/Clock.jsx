import React from 'react';


class Clock extends React.Component {

  formatSecondes = (totalSeconds) => {
    let seconds = totalSeconds % 60;
    let minutes = Math.floor(totalSeconds / 60);

    if (seconds < 10) {
      seconds = '0' + seconds;
    }
    if (minutes < 10) {
      minutes = '0' + minutes;
    }
    return minutes + ':' + seconds;
  }

  render(){
    const {totalSeconds} = this.props;
     return(
       <div>
           <span ref='text'>
             {this.formatSecondes(totalSeconds)}
           </span>
       </div>
     );
   }
};
Clock.propTypes = {
  totalSeconds: React.PropTypes.number
}
Clock.defaultProps = {
    totalSeconds: 0
}
export default Clock;
