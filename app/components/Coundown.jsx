import React from 'react';
import Clock from './Clock';
import Controls from './Controls';
import CoundownForm from './CoundownForm';

class Coundown extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      count: 0,
      countdonwnStatus: 'stopped'
    }
  }
  componentDidUpdate = (prevProps, prevState) => {
    if (this.state.countdonwnStatus !== prevState.countdonwnStatus) {
      switch (this.state.countdonwnStatus){
        case 'started':
          this.startTimer();
          break;
        case 'stopped':
          this.setState({ count: 0});
        case 'paused':
          clearInterval(this.timer);
          this.timer = undefined;
          break;
      }
    }
  }
  // componentWillUpdate = (nextProps, nextState) => {
  //   console.log('componentWillUpdate l');
  // }
  // componentWillMount = () => {
  //   console.log('componentWillMount');
  // }
  // componentDidMount = () => {
  //   console.log('componentDidMount');
  // }
  componentWillUnmount = () => {
    console.log('componentWillUnmount est appelé quand le component et suprimé du dom htm (change de component)');
    console.log(this.timer);
    clearInterval(this.timer);
    this.timer = undefined;
  }
  startTimer = () =>{
      this.timer = setInterval(() => {
        let newCount = this.state.count -1;
        this.setState({
          count: newCount >= 0 ? newCount : 0
        });
        if (newCount === 0){
          this.setState({countdonwnStatus: 'stopped'});
        }
      }, 1000);
  }
  onSetCoundown = (seconds) => {
    this.setState({
      count: seconds,
      countdonwnStatus: 'started'
    });
  }
  handleSetCountdown = (newStatus) => {
    this.setState({ countdonwnStatus: newStatus});
  }
   render(){
     const {count, countdonwnStatus} = this.state;
     var renderControlarea = () => {
       if (countdonwnStatus !== 'stopped') {
         return <Controls countdonwnStatus={countdonwnStatus} onStatusChange={this.handleSetCountdown}/>
       }else {
        return <CoundownForm onSetCoundown={this.onSetCoundown}/>
       }
     }
     return(
       <div>
        <Clock totalSeconds={count}/>
        {renderControlarea()}
       </div>
     )
   }
  };
export default Coundown;
