import React from 'react';
import Clock from './Clock';
import Controls from './Controls';

class Timer extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      count: 0,
      timerStatus: 'paused'
    }
  }
  componentDidUpdate = (prevProps, prevState) => {
    if (this.state.timerStatus !== prevState.timerStatus) {
      switch (this.state.timerStatus) {
        case 'started':
          this.handleStart();
          break;
        case 'stopped':
          this.setState({count:0});
        case 'paused':
            clearInterval(this.timer);
            this.timer = undefined;
        break;
      }
    }
  }
  componentWillUnmount = () => {
    clearInterval(this.timer);
  }
  handleStart = () => {
    this.timer = setInterval(() => {
      this.setState({
        count: this.state.count + 1
      });
    }, 1000)
  }
  handleStatusChange = (newTimerStatus) => {
    console.log(newTimerStatus);
    this.setState({ timerStatus: newTimerStatus})
  }
   render(){
     var {count, timerStatus} = this.state;

     return(
       <div>
         <h3>Time app</h3>
         <Clock totalSeconds={count}/>
         <Controls countdonwnStatus={timerStatus} onStatusChange={this.handleStatusChange} />
       </div>
     )
   }
  };

export default Timer;
