import React from 'react';

class CoundownForm extends React.Component {

  onSubmit = (e) => {
    e.preventDefault();
    let strSeconds = this.refs.seconds.value;

    if (strSeconds.match(/[0-9]*/)) {
      this.refs.seconds.value ='';
      this.props.onSetCoundown(parseInt(strSeconds, 10));
    }

  }

   render(){

     return(
       <div>
         <form ref='form' onSubmit={this.onSubmit}>
           <input type='text' ref='seconds' placeholder='Enter time in seconds'/>
           <button>start</button>
         </form>
       </div>
     )
   }
  };

export default CoundownForm;
