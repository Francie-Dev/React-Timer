import React from 'react';
import {Link, IndexLink} from 'react-router';

class Nav extends React.Component {

   render(){

     return(
       <div>
        <nav>
          <ul>
            <li>React Time App</li>
            <li><IndexLink to='/timer' activeClassName='active-link' >Timer</IndexLink></li>
            <li><Link to='/coundown' activeClassName='active-link'>Countdown</Link></li>
          </ul>
        </nav>
        <nav>
          <ul>
            <li>Created by <strong>Francis</strong></li>
          </ul>
        </nav>
       </div>
     )
   }
  };

export default Nav;
