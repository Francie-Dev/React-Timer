import React from 'react';
import Navigation from './Nav';

class App extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      name: 'isloading...',
    }
  }

   render(){
     const title = this.state.name;
     return(
       <div>
         <Navigation />
         {this.props.children}
       </div>
     )
   }
  };

export default App;
