import React from 'react';
import ReactDOM from 'react-dom';
import expect from 'expect';
import TestUtils from 'react-addons-test-utils';
import Coundown from 'Coundown';

describe('Component Coundown', () => {
  it('should existe', () => {
    expect(Coundown).toExist();
  });

  describe('test State', () => {
    it('should set state to started and countdown', (done) => {
      let countdown = TestUtils.renderIntoDocument(<Coundown/>);
      countdown.onSetCoundown(10);
      expect(countdown.state.count).toBe(10);
      expect(countdown.state.countdonwnStatus).toBe('started');

      setTimeout(() => {
        expect(countdown.state.count).toBe(9);
        done();
      }, 1001);
    });

    it('should never set count less than zero', (done) => {
      let countdown = TestUtils.renderIntoDocument(<Coundown/>);
      countdown.onSetCoundown(1);

      setTimeout(() => {
        expect(countdown.state.count).toBe(0);
        done();
      }, 1001);
    });

    it('should paused countdown on paused status', (done) => {
      var countdown = TestUtils.renderIntoDocument(<Coundown/>);
      countdown.onSetCoundown(3);
      countdown.handleSetCountdown('paused');

      setTimeout(() => {
        expect(countdown.state.count).toBe(3);
        expect(countdown.state.countdonwnStatus).toBe('paused');
        done();
      }, 1000)
    });

    it('should  resset on stopped', (done) => {
      var countdown = TestUtils.renderIntoDocument(<Coundown/>);
      countdown.onSetCoundown(0);
      countdown.handleSetCountdown('stopped');

      setTimeout(() => {
        expect(countdown.state.count).toBe(0);
        expect(countdown.state.countdonwnStatus).toBe('stopped');
        done();
      }, 1000)
    });

  });
});
