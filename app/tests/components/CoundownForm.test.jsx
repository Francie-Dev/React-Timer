import React from 'react';
import ReactDOM from 'react-dom';
import expect from 'expect';
import TestUtils from 'react-addons-test-utils';
import CoundownForm from 'CoundownForm';

describe('Component CoundownForm', () => {
  it('should existe', () => {
    expect(CoundownForm).toExist();
  });

  it('verifie setCountdown if à valid entered', () => {
    let spy = expect.createSpy();
    let coundownForm = TestUtils.renderIntoDocument(<CoundownForm onSetCoundown={spy}/>);
    let form = coundownForm.refs.form;
    coundownForm.refs.seconds.value = '109';
    TestUtils.Simulate.submit(form[0]);
    expect(spy).toHaveBeenCalledWith(109);
  });

  it('should not call onsetCountdown if invalid seconds entered', () => {
    let spy = expect.createSpy();
    let coundownForm = TestUtils.renderIntoDocument(<CoundownForm onSetCoundown={spy}/>);
    let form = coundownForm.refs.form;
    coundownForm.refs.seconds.value = '109';
    TestUtils.Simulate.submit(form[0]);
    expect(spy).toHaveBeenCalled(109);
  });

});
