import React from 'react';
import ReactDOM from 'react-dom';
import expect from 'expect';
import TestUtils from 'react-addons-test-utils';
import Clock from 'Clock';

describe('Component Clock', () => {
  it('should existe', () => {
    expect(Clock).toExist();
  });
});

describe('render', () => {
  it('should render clock to output', () => {
      let clock = TestUtils.renderIntoDocument(<Clock totalSeconds={62}/>);
      let actualText = clock.refs.text.innerHTML;
      expect(actualText).toBe('01:02');
  });
});

describe('test function formatSecondes', () => {
  it('should format seconds', () => {
    let clock = TestUtils.renderIntoDocument(<Clock/>);
    let seconds = 615;
    let expected = '10:15';
    let actual = clock.formatSecondes(seconds);
    expect(actual).toBe(expected)
  });

  it('should format seconds when min/sec are less than 10', () => {
    let clock = TestUtils.renderIntoDocument(<Clock/>);
    let seconds = 61;
    let expected = '01:01';
    let actual = clock.formatSecondes(seconds);
    expect(actual).toBe(expected)
  });


})
