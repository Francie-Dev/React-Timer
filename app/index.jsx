import React from 'react';
import ReactDOM from 'react-dom';
import {Route, Router, IndexRoute, hashHistory} from 'react-router';
// Layoute
import App from './components/App';
// Page Components
import Coundown from './components/Coundown';
import Timer from './components/Timer';

ReactDOM.render(
  <Router history={hashHistory}>
    <Route path='/' component={App}>
      <Route path='coundown' component={Coundown} />
      <Route path='timer' component={Timer} />
    </Route>
  </Router>,
  document.getElementById('root')
);
