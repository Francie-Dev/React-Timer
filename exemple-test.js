// http://jsbin.com/poyacidewa/edit?js,console
// https://github.com/mjackson/expect

console.log('Starting test ! ');

function test(t,s){
  return t+s+2;
}

expect(test(3,5)).toBe(10);
expect(test(-3,5)).toBeA('number');

function capitalizeWord(word){
  if(!word || typeof word !== 'string'){
    word = '';
  }
  return word.charAt(0).toUpperCase() + word.slice(1);
}

// verifie le result àttendu
expect(capitalizeWord('freeez')).toBe('Freeez');
// verifie le type àttendu
expect(capitalizeWord('freeez')).toBeA('string');
// given nothing expect ''
expect(capitalizeWord()).toBe('');

console.log('all tests have passed!');
